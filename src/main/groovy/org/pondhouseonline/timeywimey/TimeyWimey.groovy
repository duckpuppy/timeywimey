package org.pondhouseonline.timeywimey

class TimeyWimey implements Runnable {
	private String[] args
	private static String usage = """\
	TimeyWimey - Groovy Time Tracking

	Usage: COMMAND [OPTIONS] [ARGS...]

	COMMAND is one of:

	OTHER OPTIONS

	-h, --help
	-v, --version
	--debug

	EXAMPLES


	"""

	// Get a list of commands by searching the class's methods for any that start with "_do"
	String[] commands = TimeyWimey.metaClass.methods*.name.sort().unique()
		.findAll{it =~ /_do(.*)/}
		.collect{def m = it =~ /_do(.*)/;m[0][1].toLowerCase()}

	public static void main(String[] args) {
		def app = new TimeyWimey(args)
		//app.run()
		println app.commands
	}

	TimeyWimey(String[] args) {
		this.args = args
	}

	void run() {
		def cli = new CliBuilder(usage: "timeywimey <global-options> command <options>")
		cli.with {

		}

		def global_opts = cli.parse(args)
		def cmd = global_opts.arguments().first().toLowerCase().capitalize()
		def options = command_parser(cmd).parse(global_opts.arguments().tail())

		if (args) {
			try {
				"_do${cmd}"(args.tail())
			} catch (MissingMethodException ex) {
				throw new RuntimeException("Command '${cmd}' not recognized.")
			}
		} else {
			throw new RuntimeException("usage <command name :: 1> <args :: 0+>")
		}
	}

	void _doHelp(String[] args) {
		// TODO: Show usage text
	}
	void _doVersion(String[] args) {
		// TODO: Show application version
	}
}
