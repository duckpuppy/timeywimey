package org.pondhouseonline.timeywimey

class TimeyWimeySpec extends spock.lang.Specification {
  def t = new TimeyWimey()

  def "should have a help command"() {
    expect:
    t.commands.contains('help')
  }

  def "should have a version command"() {
    expect:
    t.commands.contains('version')
  }
}
